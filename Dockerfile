FROM python:alpine3.7
COPY . /app
WORKDIR /app
RUN apk update && apk add curl && rm -rf /var/cache/apk/*
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python ./index.py
